(function () {
	Discourse.Markdown.whiteListTag(
		'table', 
		'tbody', 
		'thead', 
		'tr', 
		'th', 
		'td', 
		'caption'
	);
}).call(this);
