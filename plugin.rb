# name: whitelist for fabrik
# about: a plugin that whitelists some HTML tags
# version: 0.1
# authors: sthzg, style suxx

register_asset "javascripts/whitelist.js", :server_side
